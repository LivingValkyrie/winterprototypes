﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LivingValkyrie.Grid;

enum MoveDirection
{
	UP,
	DOWN,
	LEFT,
	RIGHT
}

/// <summary>
/// Author: Andrew Seba
/// Description: 2D player controller.
/// </summary>
public class PlayerController : MonoBehaviour {

	[Tooltip("How fast the player will move to and from tiles.")]
	public float moveAnimationSpeed = 3f;
	[Tooltip("If you want to spawn on a specific entity assign it here.\n If nothing is assigned it will choose randomly.")]
	public GameObject spawnTile; // set to 0,0! :D

	private GameObject curTile; //current Tile positioned on.
	private GameObject targetMovementTile;
	private TwoDGridBase gridBase;
	private MoveDirection moveDir;
	private bool movePressed = false;
	//private bool moving = false;
	
	// Use this for initialization
	void Start ()
	{
		gridBase = GameObject.FindObjectOfType<TwoDGridBase>();
		if (spawnTile != null)
		{
			SpawnAndSetPosition();
		}
		else
		{
			Debug.LogWarning("No Spawn entity assigned to player prefab! Assigning random entity.");
			StartCoroutine("PickingRandomTile");
		}

		StartCoroutine("MainInputLoop");
	}

	#region SpawnStuff
	void SpawnAndSetPosition()
	{
		curTile = spawnTile;
		transform.position = curTile.transform.position;
	}

	IEnumerator PickingRandomTile()
	{
		while (true)
		{
			if (gridBase.grid != null)
			{
				List<GameObject> spawnableTiles = new List<GameObject>();
				foreach (GridNode<GameObject> node in gridBase.grid.Iterate())
				{

					if (node.entity.GetComponent<GridTile>().isWalkable)
					{
						spawnableTiles.Add(node.entity);
					}
				}
				int randNum = Random.Range(0, spawnableTiles.Count);
				spawnTile = spawnableTiles[randNum];
				SpawnAndSetPosition();
				break;
			}
			yield return null;
		}
	}
	#endregion

	IEnumerator MainInputLoop()
	{
		while (true)
		{
			if (movePressed) //Get Next Input
			{

				//Get Neighbors
				List<GameObject> neighborTiles = new List<GameObject>();
				foreach (GridNode<GameObject> node in gridBase.grid.GetNodeAt( curTile.GetComponent<GridTile>().x, curTile.GetComponent<GridTile>().y ).GetNeighbors())
				{
					if (node.entity.GetComponent<GridTile>().isWalkable)
					{
						neighborTiles.Add(node.entity);
					}
				}

				GameObject targetTile = null;

				//Move 
				if (moveDir == MoveDirection.DOWN)
				{
					foreach(GameObject tile in neighborTiles)
					{
						if (tile.GetComponent<GridTile>().x == curTile.GetComponent<GridTile>().x &&
							tile.GetComponent<GridTile>().y == curTile.GetComponent<GridTile>().y - 1)
						{
							targetTile = tile;
							break;
						}
					}
				}
				else if(moveDir == MoveDirection.UP)
				{
					foreach (GameObject tile in neighborTiles)
					{
						if (tile.GetComponent<GridTile>().x == curTile.GetComponent<GridTile>().x &&
							tile.GetComponent<GridTile>().y == curTile.GetComponent<GridTile>().y + 1)
						{
							targetTile = tile;
							break;
						}
					}
				}
				else if(moveDir == MoveDirection.LEFT)
				{
					foreach(GameObject tile in neighborTiles)
					{
						if (tile.GetComponent<GridTile>().x == curTile.GetComponent<GridTile>().x - 1 &&
							tile.GetComponent<GridTile>().y == curTile.GetComponent<GridTile>().y)
						{
							targetTile = tile;
							break;
						}
					}
				}
				else if(moveDir == MoveDirection.RIGHT)
				{
					foreach (GameObject tile in neighborTiles)
					{
						if (tile.GetComponent<GridTile>().x == curTile.GetComponent<GridTile>().x + 1 &&
							tile.GetComponent<GridTile>().y == curTile.GetComponent<GridTile>().y)
						{
							targetTile = tile;
							break;
						}
					}
				}

				
				if(targetTile != null)
				{
					yield return StartCoroutine(AnimateMovement(targetTile));
					curTile = targetTile;
				}

				movePressed = false;
			}
			yield return null;
		}
	}

	
	// Update is called once per frame
	void Update ()
	{
		GetInput();
	}

	void GetInput()
	{
		//todo store last pressed or something so that if a button is released it moves in the direction of still pressed buttons   
		if(Input.GetAxis("Vertical") < 0) //Down
		{
			moveDir = MoveDirection.DOWN;
			movePressed = true;
		}
		if(Input.GetAxis("Vertical") > 0) //Up
		{
			moveDir = MoveDirection.UP;
			movePressed = true;
		}
		if(Input.GetAxis("Horizontal") < 0) //Left
		{
			moveDir = MoveDirection.LEFT;
			movePressed = true;
		}
		if(Input.GetAxis("Horizontal") > 0)// Right
		{
			moveDir = MoveDirection.RIGHT;
			movePressed = true;
		}
	}
	

	/// <summary>
	/// Lerps the player to the target entity.
	/// </summary>
	/// <param name="target">Gameobject that we are moving too.</param>
	IEnumerator AnimateMovement(GameObject target)
	{
		Vector3 startPos = transform.position;
		float elapsedTime = 0;
		float timeNeeded = (Vector3.Distance(startPos, target.transform.position) / moveAnimationSpeed);
		if(timeNeeded != 0 && elapsedTime != timeNeeded)
		{
			while(elapsedTime < timeNeeded)
			{
				transform.position = Vector3.Lerp(startPos, target.transform.position, (elapsedTime / timeNeeded));
				elapsedTime += Time.deltaTime;
				yield return null;
			}
			transform.position = target.transform.position;
		}
	}


}
