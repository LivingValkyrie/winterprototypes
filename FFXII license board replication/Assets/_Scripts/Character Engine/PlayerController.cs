﻿using System;
using System.Collections;
using UnityEngine;

namespace LivingValkyrie.Grid {

	enum LatestDir {
		
		Horizontal, Vertical
	}

	/// <summary>
	/// Author: Matt Gipson
	/// Contact: Deadwynn@gmail.com
	/// Domain: www.livingvalkyrie.net
	/// 
	/// Description: PlayerController
	/// </summary>
	public class PlayerController : MonoBehaviour {
		#region Fields

		PlayerState state;
		InputDirection dir;
		LatestDir newDir;
		public TwoDGridBase gridBase;

		int x, y;

		#endregion

		void Start() {
			x = y = 1;
			currNode = gridBase.grid.GetNodeAt(x, y);
			transform.position = gridBase.grid.GetNodeAt(x, y).entity.transform.position;
		}

		

		void Update() {
			//only update dir on button down. always runs.
			//if (Input.GetKeyDown(KeyCode.W)) {
			//	dir = InputDirection.Up;
			//} else if (Input.GetKeyDown(KeyCode.S)) {
			//	dir = InputDirection.Down;
			//} else if (Input.GetKeyDown(KeyCode.A)) {
			//	dir = InputDirection.Left;
			//} else if (Input.GetKeyDown(KeyCode.D)) {
			//	dir = InputDirection.Right;
			//}

			if (Input.GetButtonDown("Horizontal")) {
				newDir = LatestDir.Horizontal;
			}else if (Input.GetButtonDown("Vertical")) {
				newDir = LatestDir.Vertical;
			}

			if ( Input.GetButtonUp( "Horizontal" ) ) {
				if ( Input.GetButton( "Vertical" ) ) {
					newDir = LatestDir.Vertical;
				}
			} else if ( Input.GetButtonUp( "Vertical" ) ) {
				if ( Input.GetButton( "Horizontal" ) ) {
					newDir = LatestDir.Horizontal;
				}
			}

			//todo need additional checks to have the latest input if in the same axis, so if left is pressed while moving right etc.

			switch (state) {
				case PlayerState.TakingInput:

					if (newDir == LatestDir.Horizontal) {
						if (Input.GetAxisRaw("Horizontal") == 1) {
							dir = InputDirection.Right;
						} else if (Input.GetAxisRaw("Horizontal") == -1) {
							dir = InputDirection.Left;
						} else {
							return;
						}
					} else if( newDir == LatestDir.Vertical ) {
						if ( Input.GetAxisRaw( "Vertical" ) == 1 ) {
							dir = InputDirection.Up;
						} else if (Input.GetAxisRaw("Vertical") == -1) {
							dir = InputDirection.Down;
						} else {
							return;
						}
					}

					MoveToTile(dir);

					break;
				case PlayerState.Moving:
					currentLerpTime += Time.deltaTime;
					if (currentLerpTime > lerpTime) {
						currentLerpTime = lerpTime;
					}
					float perc = currentLerpTime / lerpTime;
					transform.position = Vector3.Lerp(startPos, endPos, perc);

					if ((transform.position - endPos).magnitude < 0.01) {
						currNode = nextNode;
						x = currNode.xCoord;
						y = currNode.yCoord;
						state = PlayerState.TakingInput;
					}

					break;
				default:
					throw new ArgumentOutOfRangeException();
			}
		}

		public float lerpTime = 0.5f;
		float currentLerpTime;

		Vector3 startPos;
		Vector3 endPos;
		GridNode<GameObject> currNode, nextNode;

		void MoveToTile(InputDirection dir) {
			if (SetupMovementValues(dir)) {
				state = PlayerState.Moving;
			} else {
				print("cant move that way " + dir.ToString());
			}
		}

		bool SetupMovementValues(InputDirection dir) {
			bool canMove = false;
			currentLerpTime = 0;
			startPos = currNode.entity.transform.position;

			GridNode<GameObject> node;
			switch (dir) {
				case InputDirection.Up:
					node = gridBase.grid.GetNodeAt(x, y + 1);
					break;
				case InputDirection.Down:
					node = gridBase.grid.GetNodeAt(x, y - 1);

					break;
				case InputDirection.Left:
					node = gridBase.grid.GetNodeAt(x - 1, y);

					break;
				case InputDirection.Right:
					node = gridBase.grid.GetNodeAt(x + 1, y);

					break;
				default:
					throw new ArgumentOutOfRangeException("dir", dir, null);
			}

			if (node != null) {
				nextNode = node;
				if (node.entity.GetComponent<GridTile>().isWalkable) {
					canMove = true;
					endPos = node.entity.transform.position;
				}
			}

			return canMove;
		}
	}

	enum PlayerState {
		TakingInput,
		Moving
	}

	enum InputDirection {
		Up,
		Down,
		Left,
		Right
	}
}