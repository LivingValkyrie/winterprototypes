﻿using UnityEngine;

[CreateAssetMenu]
public class ConditionTester : Condition {
	public bool toReturn;

	internal override bool Evaluate() {
		Debug.Log("Test Passed");
		return toReturn;
	}
}