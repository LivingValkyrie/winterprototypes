﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Condition
/// </summary>
public abstract class Condition : ScriptableObject {
	#region Fields

	#endregion

	internal abstract bool Evaluate();

	public static implicit operator bool( Condition value ) {
		// returns the evaluate method
		return value.Evaluate();
	}
}
