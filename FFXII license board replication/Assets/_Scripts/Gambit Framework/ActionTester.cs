﻿using UnityEngine;

[CreateAssetMenu]
public class ActionTester : Action {

	public bool toReturn;

	internal override bool Perform() {
		Debug.Log("Performed");
		return toReturn;
	}
}