﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: Action
/// </summary>
public abstract class Action : ScriptableObject {
	#region Fields

	#endregion

	internal abstract bool Perform();

	public static implicit operator bool( Action value ) {
		// returns the evaluate method
		return value.Perform();
	}
}
