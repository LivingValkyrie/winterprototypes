﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: GambitAgent
/// </summary>
public class GambitAgent : MonoBehaviour {
	#region Fields

	public Gambit[] gambits;
	public float gambitUpdatesPerSecond = 1;
	float gambitTimer;

	#endregion

	void Start() {
		gambitTimer = 1f / gambitUpdatesPerSecond;

		InvokeRepeating("UpdateGambits", 0, gambitTimer);
	}

	void UpdateGambits() {
		foreach (Gambit g in gambits) {
			if (g.condition) {
				if (g.action) {
					return;
				}
			}
		}
	}
}

[System.Serializable]
public struct Gambit {
	public Condition condition;
	public Action action;
}