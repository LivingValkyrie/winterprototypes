﻿using LivingValkyrie.Grid;
using UnityEngine;
using UnityEditor;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.com
/// 
/// Description: TwoDGridBase 
/// </summary>
public class TwoDGridBase : MonoBehaviour {
	#region Fields

	//public GameObject[,] grid;
	public int width;
	public int height;
	public float tileSize;
	public Sprite tileSprite;
	public GameObject[] oneDGrid;
	public Grid<GameObject> grid;

	#endregion

	public void Awake() {
		//grid = GridHelper.GenerateGrid<GameObject>(width, height);
		//foreach (var node in GridHelper.IterateGridWithCoords(grid)) {
		//	node.Value.transform.position = new Vector3(node.Key.x * tileSize, node.Key.y * tileSize);
		//	node.Value.AddComponent<SpriteRenderer>().sprite = tileSprite;
		//	var temp = node.Value.AddComponent<GridTile>();
		//	temp.x = (int)node.Key.x;
		//	temp.y = (int)node.Key.y;

		//	node.Value.name = string.Format("Node {0} {1}", temp.x, temp.y);
		//}


		//oneDGrid = GridHelper.ConvertTo1D<GameObject>(grid);

		//for (int i = 0; i < oneDGrid.Length; i++) {
		//	GameObject o = oneDGrid[i];
		//	//print(o.name + " " + i);
		//}

		grid = GridHelper.GenerateFlattenedGrid<GameObject>(width, height);
		GameObject parent = new GameObject("Parent");
		foreach (var node in grid.Iterate()) {
			node.entity.transform.position = new Vector3(node.xCoord, node.yCoord);
			node.entity.AddComponent<SpriteRenderer>().sprite = tileSprite;
			var tile = node.entity.AddComponent<GridTile>();
			tile.x = node.xCoord;
			tile.y = node.yCoord;
			tile.name = string.Format( "Node {0} {1}", node.xCoord, node.yCoord );
			node.entity.transform.SetParent(parent.transform);
		}

		foreach ( GridNode<GameObject> node in grid.GetEdgeNodes() ) {
			node.entity.GetComponent<GridTile>().isWalkable = false;
		}

		foreach (var neighbor in grid.GetNodeAt(1,1).GetNeighbors()) {
			neighbor.entity.GetComponent<SpriteRenderer>().color = Color.green; 
		}

	}

	public bool debug = false;

	void OnDrawGizmos() {
#if UNITY_EDITOR
		if (Application.isPlaying && debug) {
			foreach (var VARIABLE in grid.Iterate()) {
				int walkable = 0;

				if (VARIABLE.entity.GetComponent<GridTile>().isWalkable) {
					walkable = 1;
				}

				Vector3 pos = new Vector3(VARIABLE.xCoord * tileSize - (tileSize * 0.25f), VARIABLE.yCoord * tileSize + (tileSize * 0.3f));

				Handles.Label(pos, walkable.ToString());
			}
		}
#endif
	}
}