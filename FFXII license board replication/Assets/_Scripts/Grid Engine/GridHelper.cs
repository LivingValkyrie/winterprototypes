﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace LivingValkyrie.Grid {

	public enum NeighborType {
		Vertical, //top and bottom neighbors
		Horizontal, //left and right neighbors
		Cross, //horizontal and vertical neighbors
		Diagonal, //diagonal neighbors
		All //diagonal and cross
	}

	/// <summary>
	/// Author: Matt Gipson
	/// Contact: Deadwynn@gmail.com
	/// Domain: www.livingvalkyrie.com
	/// 
	/// Description: GridHelper is a class with grid based functions made to work with generics. 
	/// </summary>
	public static class GridHelper {

		public static TGridType[,] GenerateGrid<TGridType>(int width, int height) where TGridType : new() {
			TGridType[,] arrayToReturn = new TGridType[width, height];

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					arrayToReturn[x, y] = new TGridType();
				}
			}

			return arrayToReturn;
		}

		public static Grid<TGridType> GenerateFlattenedGrid<TGridType>(int width, int height) where TGridType : new() {
			TGridType[,] arrayToReturn = new TGridType[width, height];

			for (int x = 0; x < width; x++) {
				for (int y = 0; y < height; y++) {
					arrayToReturn[x, y] = new TGridType();
				}
			}

			return new Grid<TGridType>(ConvertTo1D(arrayToReturn), width, height);
		}

		public static List<TGridType> FindNeighbors<TGridType>(TGridType[,] grid, int gridX, int gridY, NeighborType type, int radius = 1) {
			List<TGridType> neighborsToReturn = new List<TGridType>();

			for (int x = -radius; x <= radius; x++) {
				for (int y = -radius; y <= radius; y++) {
					if (x == 0 && y == 0) {
						continue; //inside current node
					}

					int checkX = gridX + x;
					int checkY = gridY + y;

					//check if in bounds
					if (checkX >= 0 && checkX < grid.GetLength(0) && checkY >= 0 && checkY < grid.GetLength(1)) {
						switch (type) {
							case NeighborType.Vertical:

								//same vertical plane
								if (x == 0) {
									neighborsToReturn.Add(grid[checkX, checkY]);
								}
								break;
							case NeighborType.Horizontal:

								//same horizontal plane
								if (y == 0) {
									neighborsToReturn.Add(grid[checkX, checkY]);
								}
								break;
							case NeighborType.Cross:

								//vertical and horizontal planes are both the same
								if (x == 0 || y == 0) {
									neighborsToReturn.Add(grid[checkX, checkY]);
								}
								break;
							case NeighborType.Diagonal:

								//test if location has the same x and y compared to the base node
								if (Math.Abs(checkX - gridX) == Math.Abs(checkY - gridY)) {
									neighborsToReturn.Add(grid[checkX, checkY]);
								}
								break;
							case NeighborType.All:

								//add all results
								neighborsToReturn.Add(grid[checkX, checkY]);
								break;
						}
					}
				}
			}

			//return
			return neighborsToReturn;
		}

		public static List<GridNode<TGridType>> FindNeighbors<TGridType>(Grid<TGridType> grid, int gridX, int gridY, NeighborType type, int radius = 1) {
			List<GridNode<TGridType>> neighborsToReturn = new List<GridNode<TGridType>>();

			for (int x = -radius; x <= radius; x++) {
				for (int y = -radius; y <= radius; y++) {
					if (x == 0 && y == 0) {
						continue; //inside current node
					}

					int checkX = gridX + x;
					int checkY = gridY + y;

					//check if in bounds
					if (checkX >= 0 && checkX < grid.width && checkY >= 0 && checkY < grid.height) {
						switch (type) {
							case NeighborType.Vertical:

								//same vertical plane
								if (x == 0) {
									neighborsToReturn.Add(grid.GetNodeAt(checkX, checkY));
								}
								break;
							case NeighborType.Horizontal:

								//same horizontal plane
								if (y == 0) {
									neighborsToReturn.Add(grid.GetNodeAt(checkX, checkY));
								}
								break;
							case NeighborType.Cross:

								//vertical and horizontal planes are both the same
								if (x == 0 || y == 0) {
									neighborsToReturn.Add(grid.GetNodeAt(checkX, checkY));
								}
								break;
							case NeighborType.Diagonal:

								//test if location has the same x and y compared to the base node
								if (Math.Abs(checkX - gridX) == Math.Abs(checkY - gridY)) {
									neighborsToReturn.Add(grid.GetNodeAt(checkX, checkY));
								}
								break;
							case NeighborType.All:

								//add all results
								neighborsToReturn.Add(grid.GetNodeAt(checkX, checkY));

								break;
						}
					}
				}
			}

			return neighborsToReturn;
		}

		public static IEnumerable<TGridType> IterateGrid<TGridType>(TGridType[,] grid) {
			for (int x = 0; x < grid.GetLength(0); x++) {
				for (int y = 0; y < grid.GetLength(1); y++) {
					yield return grid[x, y];
				}
			}
		}

		public static IEnumerable<KeyValuePair<Vector2, TGridType>> IterateGridWithCoords<TGridType>(TGridType[,] grid) {
			for (int x = 0; x < grid.GetLength(0); x++) {
				for (int y = 0; y < grid.GetLength(1); y++) {
					yield return new KeyValuePair<Vector2, TGridType>(new Vector2(x, y), grid[x, y]);
					;
				}
			}
		}

		public static List<TGridType> GetEdgeNodes<TGridType>(TGridType[,] grid) {
			List<TGridType> edgeNodes = new List<TGridType>();
			foreach (var type in IterateGridWithCoords(grid)) {
				if (type.Key.x == 0 || type.Key.x == grid.GetLength(0) - 1) {
					edgeNodes.Add(grid[(int) type.Key.x, (int) type.Key.y]);
				} else if (type.Key.y == 0 || type.Key.y == grid.GetLength(1) - 1) {
					edgeNodes.Add(grid[(int) type.Key.x, (int) type.Key.y]);
				}
			}
			return edgeNodes;
		}

		public static TGridType[,] ConvertTo2D<TGridType>(TGridType[] gridToConvert) {
			return null;
		}

		public static TGridType[] ConvertTo1D<TGridType>(TGridType[,] gridToConvert) {
			var newGrid = new TGridType[gridToConvert.GetLength(0) * gridToConvert.GetLength(1)];

			foreach (var node in IterateGridWithCoords(gridToConvert)) {
				newGrid[(int) node.Key.x * gridToConvert.GetLength(0) + (int) node.Key.y] = node.Value;
			}

			return newGrid;
		}
	}
}