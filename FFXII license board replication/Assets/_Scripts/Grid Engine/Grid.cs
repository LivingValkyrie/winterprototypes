﻿using System.Collections.Generic;
using UnityEngine;

namespace LivingValkyrie.Grid {

	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TGridType"></typeparam>
	public class Grid<TGridType> {
		public int width, height;
		GridNode<TGridType>[] collection;

		public Grid(int width, int height) {
			this.width = width;
			this.height = height;

			collection = new GridNode<TGridType>[width * height];
		}

		//todo needs a contructor that generates its own collection
		public Grid(TGridType[] collection, int width, int height) {
			//Debug.Log("called");
			this.width = width;
			this.height = height;

			this.collection = new GridNode<TGridType>[collection.Length];
			for (int i = 0; i < collection.Length; i++) {
				this.collection[i] = new GridNode<TGridType>(collection[i], i, width, this);
			}
		}

		public IEnumerable<GridNode<TGridType>> GetEdgeNodes() {
			List<GridNode<TGridType>> edgeNodes = new List<GridNode<TGridType>>();
			foreach (var node in Iterate()) {
				if (node.xCoord == 0 || node.xCoord == width - 1) {
					edgeNodes.Add(node);
				} else if (node.yCoord == 0 || node.yCoord == height - 1) {
					edgeNodes.Add(node);
				}
			}
			return edgeNodes;
		}

		public GridNode<TGridType> GetNodeAt(int x, int y ) {
			int index = (y * width) + x;
			if (index >= 0 && index < collection.Length) {
				return collection[index];
			} else {
				Debug.Log(string.Format("x {0} y{1} is null", x, y));
			}

			return null;
		}

		public void GetNodeAt( int x, int y, out GridNode<TGridType> node ) {
			int index = ( y * width ) + x;
			if ( index >= 0 && index < collection.Length ) {
				node = collection[index];
			} else {
				Debug.Log( string.Format( "x {0} y{1} is null", x, y ) );
			}

			node = null;
		}

		public IEnumerable<GridNode<TGridType>> Iterate() {
			for (int y = 0; y < height; y++) {
				for (int x = 0; x < width; x++) {
					yield return GetNodeAt(x, y);
				}
			}
		}

	}

	/// <summary>
	/// 
	/// </summary>
	/// <typeparam name="TGridType"></typeparam>
	[System.Serializable]
	public class GridNode<TGridType> {
		#region Neighbors

		public GridNode<TGridType> LeftNeighbor {
			get { return grid.GetNodeAt(xCoord - 1, yCoord); }
		}
		public GridNode<TGridType> RightNeighbor {
			get { return grid.GetNodeAt(xCoord + 1, yCoord); }

		}
		public GridNode<TGridType> TopNeighbor {
			get { return grid.GetNodeAt(xCoord, yCoord + 1); }

		}
		public GridNode<TGridType> BottomNeighbor {
			get { return grid.GetNodeAt(xCoord, yCoord - 1); }

		}
		public GridNode<TGridType> TopLeftNeighbor {
			get { return grid.GetNodeAt(xCoord - 1, yCoord + 1); }

		}
		public GridNode<TGridType> TopRightNeighbor {
			get { return grid.GetNodeAt(xCoord + 1, yCoord + 1); }

		}
		public GridNode<TGridType> BottomLeftNeighbor {
			get { return grid.GetNodeAt(xCoord - 1, yCoord - 1); }

		}
		public GridNode<TGridType> BottomRightNeighbor {
			get { return grid.GetNodeAt(xCoord + 1, yCoord - 1); }

		}

		#endregion

		/// <summary>
		/// The entity that holds this grid node instance, such as a unity component, should be set in start or awake for unity or in the constructor in standard c#.
		/// </summary>
		public TGridType entity;

		/// <summary>
		/// the grid that this node is a member of.
		/// </summary>
		public Grid<TGridType> grid;

		public int xCoord, yCoord;

		/// <summary>
		/// Initializes a new instance of the <see cref="GridNode{TGridType}"/> class.
		/// </summary>
		/// <param name="entity">The entity that holds this instance.</param>
		public GridNode(TGridType entity) {
			this.entity = entity;
		}

		/// <summary>
		/// Initializes a new instance of the <see cref="GridNode{TGridType}"/> class.
		/// </summary>
		/// <param name="entity">The entity.</param>
		/// <param name="index">The index.</param>
		/// <param name="collectionWidth">Width of the collection.</param>
		/// <param name="grid">The grid.</param>
		public GridNode(TGridType entity, int index, int collectionWidth, Grid<TGridType> grid) {
			this.entity = entity;
			yCoord = index / collectionWidth;
			xCoord = index % collectionWidth;
			this.grid = grid;

			//Debug.Log(this);
		}

		public IEnumerable<GridNode<TGridType>> GetNeighbors() {
			yield return LeftNeighbor;
			yield return RightNeighbor;
			yield return TopNeighbor;
			yield return BottomNeighbor;
			yield return BottomLeftNeighbor;
			yield return BottomRightNeighbor;
			yield return TopLeftNeighbor;
			yield return TopRightNeighbor;
		}

		public override string ToString() {
			return string.Format("Node at {0} {1} is {2}", xCoord, yCoord, entity);
		}

	}
}