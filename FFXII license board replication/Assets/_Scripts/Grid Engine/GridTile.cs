﻿using UnityEngine;

/// <summary>
/// Author: Matt Gipson
/// Contact: Deadwynn@gmail.com
/// Domain: www.livingvalkyrie.net
/// 
/// Description: GridTile
/// </summary>
public class GridTile : MonoBehaviour {
	#region Fields

	public bool isWalkable = true;
	public int x, y;

	#endregion

}